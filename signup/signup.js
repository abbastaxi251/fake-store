const loginTab = document.getElementById("login-tab")
const signupTab = document.getElementById("signup-tab")
const loginForm = document.getElementById("login-form")
const signupForm = document.getElementById("signup-form")
const showSignup = document.getElementById("show-signup")
const showLogin = document.getElementById("show-login")
const errorMessaging = document.getElementsByClassName("error-messaging")
const pwdMatchFail = document.getElementsByClassName("pwd-match-fail")
const userNotFound = document.getElementsByClassName("user-not-found")
const incorrectInfo = document.getElementsByClassName("incorrect-info")

loginTab.addEventListener("click", () => {
    loginTab.classList.add("active")
    signupTab.classList.remove("active")
    loginForm.classList.remove("hidden")
    signupForm.classList.add("hidden")
})

signupTab.addEventListener("click", () => {
    signupTab.classList.add("active")
    loginTab.classList.remove("active")
    signupForm.classList.remove("hidden")
    loginForm.classList.add("hidden")
})

showSignup.addEventListener("click", () => {
    signupTab.click()
})

showLogin.addEventListener("click", () => {
    loginTab.click()
})

signupForm.addEventListener("submit", (e) => {
    e.preventDefault()
    const name = document.getElementById("signup-name").value
    const email = document.getElementById("signup-email").value
    let password = null
    let isLoggedIn = 0
    if (
        document.getElementById("signup-password").value ===
        document.getElementById("signup-confirm-password").value
    ) {
        password = document.getElementById("signup-password").value
        isLoggedIn = 1
        const userInfoFakeStore = {
            name: name,
            password: password,
            isLoggedIn: isLoggedIn,
        }
        localStorage.setItem(String(email), JSON.stringify(userInfoFakeStore))
        localStorage.setItem("email", String(email))
        window.location.href = "../index.html"
    } else {
        errorMessaging[0].style.display = "flex"
        pwdMatchFail[0].style.display = "flex"
    }
})

loginForm.addEventListener("submit", (e) => {
    e.preventDefault()

    const email = document.getElementById("login-email").value
    const password = document.getElementById("login-password").value

    const userInfoFakeStore = JSON.parse(localStorage.getItem(String(email)))

    if (password == userInfoFakeStore.password) {
        userInfoFakeStore.isLoggedIn = 1
        localStorage.setItem(String(email), JSON.stringify(userInfoFakeStore))
        localStorage.setItem("email", String(email))
        window.location.href = "../index.html"
    } else {
        incorrectInfo[0].style.display = "flex"
    }
})
