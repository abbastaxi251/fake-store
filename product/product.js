const productId = localStorage.getItem("id")

document.addEventListener("DOMContentLoaded", async () => {
    try {
        const response = await fetch(
            `https://fakestoreapi.com/products/${productId}`
        )
        const product = await response.json()
        // console.log(product)

        let loader = document.getElementById("loading")
        loader.style.display = "none"

        if (product.length == 0) {
            let noProducts = document.getElementsByClassName("no-products")
            noProducts[0].style.display = "flex"
        }

        viewProduct(product)

        let backBtn = document.getElementsByClassName("back-to-home")
        backBtn[0].addEventListener("click", () => {
            window.location.href = "../index.html"
        })
    } catch (error) {
        console.error(error)
    }
})

const viewProduct = (product) => {
    let imageContainer = document.getElementsByClassName("image-container")
    image = document.createElement("img")
    image.className = "product-image"
    image.src = product.image
    imageContainer[0].appendChild(image)

    let titleContainer = document.getElementsByClassName("title")
    titleContainer[0].textContent = product.title

    let rating = document.getElementsByClassName("rating")
    rating[0].textContent = product.rating.rate

    let count = document.getElementsByClassName("count")
    count[0].textContent = product.rating.count

    let starImg = document.getElementsByClassName("star-img")
    let starImage = document.createElement("img")
    starImage.src = "../star.png"
    starImg[0].appendChild(starImage)

    let price = document.getElementsByClassName("price")
    price[0].textContent = product.price

    let description = document.getElementsByClassName("description")
    description[0].textContent = product.description
}
