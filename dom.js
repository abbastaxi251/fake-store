document.addEventListener("DOMContentLoaded", async () => {
    try {
        // CHECK IF USER LOGGED IN
        const userInfoFakeStore = JSON.parse(
            localStorage.getItem(localStorage.getItem("email"))
        )
        console.log(userInfoFakeStore)

        if (userInfoFakeStore == null || userInfoFakeStore.isLoggedIn == 0) {
            window.location.href = "./signup/signup.html"
        }

        // FETCH PRODUCTS IF USER LOGGED IN
        const response = await fetch("https://fakestoreapi.com/products")

        if (!response.ok) {
            let fetchFail = document.querySelector(".fetch-fail")
            fetchFail.style.display = "flex"
            fetchFail.style.justifyContent = "center"
            fetchFail.style.alignItems = "center"
        }

        const products = await response.json()

        let loader = document.getElementById("loading")
        loader.style.display = "none"

        if (products.length == 0) {
            let noProducts = document.getElementsByClassName("no-products")
            noProducts[0].style.display = "flex"
        }

        const categories = []

        for (let product of products) {
            createProductElement(product, categories)
        }
    } catch (error) {
        console.error(error)

        // DISPLAY FETCH FAIL ON THE SCREEN
        let loader = document.getElementById("loading")
        loader.style.display = "none"

        let noProducts = document.getElementsByClassName("no-products")
        noProducts[0].style.display = "none"

        let fetchFail = document.querySelector(".fetch-fail")
        fetchFail.style.display = "block"
    }
})

const createProductElement = (product, categories) => {
    if (!categories.includes(product.category)) {
        categories.push(product.category)

        let categoryContainer = document.createElement("div")
        categoryContainer.className =
            String(product.category).split(" ").join("-") + " category"

        let mainContainer = document.getElementsByClassName("categories")
        mainContainer[0].appendChild(categoryContainer)

        let filterListContainer =
            document.getElementsByClassName("categories-list")

        let category = document.createElement("li")
        category.textContent = product.category

        let checkbox = document.createElement("input")
        checkbox.type = "checkbox"
        checkbox.className = "check"

        let filterCategoryContainer = document.createElement("div")
        filterCategoryContainer.appendChild(checkbox)
        filterCategoryContainer.appendChild(category)
        // console.log(filterListContainer)
        filterListContainer[0].appendChild(filterCategoryContainer)
    }
    let categoryClass = String(product.category).split(" ").join("-")
    let categoryContainer = document.getElementsByClassName(categoryClass)

    // CREATE A PRODUCT CONTAINER FOR THE PRODUCT

    let productContainer = document.createElement("div")
    productContainer.className = "product"

    let imageContainer = document.createElement("div")
    imageContainer.className = "image-container"

    let image = document.createElement("img")
    image.className = "product-image"
    image.src = product.image

    imageContainer.appendChild(image)

    let titleContainer = document.createElement("div")
    titleContainer.className = "title"
    titleContainer.textContent = product.title

    let ratingContainer = document.createElement("div")
    ratingContainer.className = "rating-container"

    let ratings = document.createElement("div")
    ratings.className = "ratings"
    ratings.textContent = product.rating.rate

    let starImage = document.createElement("img")
    starImage.className = "star-image"
    starImage.src = "./star.png"

    ratingContainer.appendChild(ratings)
    ratingContainer.appendChild(starImage)

    let priceContainer = document.createElement("div")
    priceContainer.className = "price-container"

    let dollarConatainer = document.createElement("div")
    dollarConatainer.className = "dollar"
    dollarConatainer.textContent = "&dollar;" + product.price
    priceContainer.appendChild(dollarConatainer)
    // priceContainer.innerHTML =
    //     "<div class='dollar'>&dollar;</div>" + product.price

    productContainer.appendChild(imageContainer)
    productContainer.appendChild(titleContainer)
    productContainer.appendChild(ratingContainer)
    productContainer.appendChild(priceContainer)
    categoryContainer[0].appendChild(productContainer)

    productContainer.addEventListener("click", () => {
        localStorage.setItem("id", product.id)
        window.location.href = "./product/product.html"
    })
}
